//
//  ViewController.m
//  DGSDKDemo
//
//  Created by Marco on 12/11/18.
//  Copyright © 2018 DG. All rights reserved.
//

#import "ViewController.h"
#import "AFNetworking.h"

#import <DGSDK/DGSDKHandle.h>

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    
    
    //监听SDK推送=----- 可选择
    //准备进入SDK
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(DGSDKWillEnterGame) name:@"DGSDKWILLENTERGAME" object:nil];
    //进入SDK -- token登录成功
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(DGSDKEnteredGame) name:@"DGSDKENTEREDGAME" object:nil];
   
     //已经退出SDK
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(DGSDKExitedGame) name:@"DGSDKEXITEDGAME" object:nil];
    
    //进入SDK失败
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(DGSDKEnterGameFail:) name:@"DGSDKENTEREDGAMEFAIL" object:nil];
    
}

#pragma mark - 监听SDK推送
- (void)DGSDKWillEnterGame{
    NSLog(@"即将进入SDK");
}
- (void)DGSDKEnteredGame{
    NSLog(@"已经进入SDK ");
}

//失败回调
/*
 数据加载失败的回调，i 有三个类型
 SocketNetError = 0;    socket链接失败
 MemberInitError = 1;   会员初始化失败
 MemberStop = 2;        会员账号暂停
 */
- (void)DGSDKEnterGameFail:(NSNotification *)notifier{
    NSDictionary *dicError = notifier.object;
    int code = [dicError[@"DGError"] intValue];
    NSLog(@"进入SDK失败。。。。 %d",code);
}




- (void)DGSDKExitedGame{
    NSLog(@"已经退出SDK");
}

#pragma mark - Action
- (IBAction)start_action:(id)sender {
    //请求获取Token， 请求获取配置文件，登录api有提供
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    //1. 请求获取配置文件,demo 请求game_info.json，以登录api有提供为准
    [manager GET:@"https://static.dg012.com/game_info.json" parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *dic = responseObject;
        NSLog(@"responseObject:%@",dic);
        if (responseObject) {
            //2. 接入SDK ,传入Token
            [manager POST:@"http://www.dg99.asia/game/h5" parameters:nil progress:^(NSProgress * _Nonnull uploadProgress) {
            } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject1) {
                NSDictionary *dic = responseObject1;
                NSLog(@"responseObject:%@",dic);
                if (dic) {
                    NSString *token = dic[@"token"];
                    if (token!=nil) {
                        //3.接入SDK
                        [DGSDKHANDLE DGSDKLoginWithToken:token withViewController:self withLoadDGGameLoading:YES Data:responseObject];
                    }
                }
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            }];
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
    }];
}



@end
