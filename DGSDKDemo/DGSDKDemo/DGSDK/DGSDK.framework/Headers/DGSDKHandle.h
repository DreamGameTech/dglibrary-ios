//
//  DGSDKHandle.h
//  DG
//
//  Created by Marco on 12/11/18.
//  Copyright © 2018 Samnang. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

///消息通知
//即将进入SDK
#define DGSDKWILLENTERGAME @"DGSDKWILLENTERGAME"
//进入SDK -- token登录成功
#define DGSDKENTEREDGAME @"DGSDKENTEREDGAME"
//已经退出SDK
#define DGSDKEXITEDGAME @"DGSDKEXITEDGAME"


//进入SDK失败
#define DGSDKENTEREDGAMEFAIL @"DGSDKENTEREDGAMEFAIL"
/*
 key:DGError
 
 数据加载失败的回调，i 有三个类型
 SocketNetError = 0;    socket链接失败
 MemberInitError = 1;   会员初始化失败
 MemberStop = 2;        会员账号暂停
 
 
 
 */

//跳转到对应的厅
typedef NS_ENUM(NSInteger,DGLobbyType)

{
    DGLobbyTypeDream,       //梦幻旗舰厅
    DGLobbyTypeBerwick,     //皇冠巴域厅
    DGLobbyTypeBobe,        //皇冠波贝厅
    DGLobbyTypeJingmi,      //梦幻竞咪厅
    DGLobbyTypeMore         //多桌百家乐
};





//----
#define DGSDKHANDLE [DGSDKHandle sharedInstance]

@interface DGSDKHandle : NSObject

+(instancetype)sharedInstance;

//使用token登录DG Game, 状态使用通知监听
/*
 token:需要登录用户token
 VC:当前的ViewController
 isLoadDGGame: 显示SDK加载Toast    YES显示， False不显示
  configData: 为对接是请求返回的json字符串
 
 */
- (void)DGSDKLoginWithToken:(NSString *)token
         withViewController:(UIViewController *)VC
      withLoadDGGameLoading:(BOOL)isShowHUD
                       Data:(NSString *)configData;

//跳转到指定的大厅 ---- 可选
- (void)DGSDKLobbyWithType:(DGLobbyType)type;

//!!!!!  对接平台退出SDK，账号踢出，谨慎调用
- (void)DGSDKAccountLogout;

@end
