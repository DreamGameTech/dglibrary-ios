## DGLibray-iOS 是DreamGame Technology 公司游戏产品的SDK，供平台商对接我司游戏使用

## 该SDK包含DG所有游戏大厅和游戏种类。<br> 支持iOS 9以上，iPhone、iPad的armv7、arm64架构

### 文档修改日志

版本号 | 修改内容 | 修改日期
----|------|----
2.0.3 | 兼容新版聊天功能| 2020-01-03
2.0.2 | 登录接口添加配置文件传入，对接登录API，返回配置文件字符串，传入SDK   | 2019-11-04
2.0.1 | iPhone 11系列 兼容| 2019-10-25
2.0.0 | HttpDNS已内置， 旧版本可删除HttpDNS库  | 2019-10-17

### 对接方式
    
#### 1. 添加配置
(1) TARGETS：
             framework使用视频框架，需引入系统框架<b>VideoToolbox.framework  </b>


(2) info.plist ：
             使用到http的请求
             
```
App Transport Security Settings     (Type:Dictionary)   
-> Allow Arbitrary Loads (Type:Boolean Value:YES)
```

(3) Build Settings ：
             由于framework的配置， 需要添加配置
             
```
Linking 
    >Other Linker Flags    添加： -ObjC
    
添加库
TARGETS->build Phases->Link Binary With Libraries  添加库libresolv   
```      

### 2.引入SDK
相关文件在DGSDK 目录内，请前往查看。    
需要把MJRefresh.bundle、DGSDK.bundle、DGSDKResources.bundle、DGSDK.framework 拖入工程。   
<b>备注DGSDK.bundle 、MJRefresh.bundle 在DGSDK.framework内</b>

```
MJRefresh.bundle
DGSDK.bundle
DGSDKResources.bundle
DGSDK.framework 



----- 2.0版本以上， httpDNS已内置， 可删除httpDNS库 -----
最新引入HttpDNS ， 整个HttpDns文件夹 拖入工程，假如提示失败，请到工程配置查看路径是否正确，
Seaerch Paths -> Framework Search Paths

AlicloudHttpDNS.framework 
UTMini.framework 
AlicloudUtils.framework 
AlicloudBeacon.framework 
UTDID.framework 
--------------------------------------------
```     
#### 3. 代码接入SDK

(1) 引入SDK文件：

```
#import <DGSDK/DGSDKHandle.h>	
```


(2) 监听SDK消息通知    <b>---可选</b>
> DGSDKWILLENTERGAME：即将进入SDK   
 DGSDKENTERGAME：已经进入SDK    
 DGSDKEXITEDGAME  ：已经退出SDK	
 DGSDKENTEREDGAMEFAIL   ：进入SDK失败 -- 有值回调
 
> 数据加载失败的回调，i 有三个类型   
> key:DGError  
> 0 --- socket链接失败   
> 1 --- 会员初始化失败   
> 2 --- 会员账号暂停	
	

(3) 调用SDK方法   
	
> token:需要登录用户token   
 VC:当前的ViewController   
 isLoadDGGame: 显示SDK加载Toast    YES显示， False不显示  
   configData: 为对接是请求返回的json字符串

 
```
 - (void)DGSDKLoginWithToken:(NSString *)token
         withViewController:(UIViewController *)VC
      withLoadDGGameLoading:(BOOL)isShowHUD
                       Data:(NSString *)configData;
      
```  

实例：  

```
 [DGSDKHANDLE DGSDKLoginWithToken:token
              withViewController:self
        withLoadDGGameLoading:YES
                        Data:domains];
```  

(4)进入对应的游戏大厅，默认是：梦幻旗舰厅         <b>---可选</b>
> DGLobbyTypeDream： 梦幻旗舰厅    
DGLobbyTypeBerwick： 皇冠巴域厅   
DGLobbyTypeBobe：    皇冠波贝厅   
DGLobbyTypeJingmi：  梦幻竞咪厅   
DGLobbyTypeMore：    多桌百家乐  

```
 [DGSDKHANDLE DGSDKLobbyWithType:DGLobbyTypeDream];
```



